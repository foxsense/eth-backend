// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";

contract Music is ERC721Enumerable{
  struct MusicData {
    uint tokenId;
    string name;
    uint price;
    bool isSold;
    address soldTo;
    uint soldAmount;
  }
  struct Bid {
    uint bidId;
    address biddedBy;
    uint amount;
  }

  mapping(uint => Bid[]) public _bids;
  mapping(uint => MusicData) public _music;

  constructor() ERC721("Music", "MUSIC") ERC721Enumerable() public {
  }

  function _transferAmount(address payable addressData, uint amount) internal {
    addressData.transfer(amount * 1000000000000000000);
  }

  function _transferToken(uint tokenId, address buyer, uint amount) internal {
    _music[tokenId].isSold = true;
    _music[tokenId].soldTo = buyer;
    _music[tokenId].soldAmount = amount;
    _transferAmount(payable(msg.sender), amount);
    _safeTransfer(msg.sender, buyer, tokenId, "");
  }

  function getMusic(uint tokenId) view public returns (uint, string memory, uint, bool, address, uint) {
    require(tokenId < ERC721Enumerable.totalSupply(), "Index out of bounds");
    MusicData memory musicData = _music[tokenId];
    return (musicData.tokenId, musicData.name, musicData.price, musicData.isSold, musicData.soldTo, musicData.soldAmount);
  }

  function totalBidsCount(uint tokenId) view public returns (uint) {
    require(tokenId < ERC721Enumerable.totalSupply(), "Index out of bounds");
    return _bids[tokenId].length;
  }

  function getBid(uint tokenId, uint bidId) view public returns (uint, address, uint) {
    require(tokenId < ERC721Enumerable.totalSupply(), "Index out of bounds");
    Bid[] memory bids = _bids[tokenId];
    require(bidId < bids.length, "Index out of bounds");
    return (bids[bidId].bidId, bids[bidId].biddedBy, bids[bidId].amount);
  }

  function mint(string memory name, uint price) public {
    uint totalTokens = ERC721Enumerable.totalSupply();
    _music[totalTokens] = MusicData(totalTokens, name, price, false, address(0), 0);
    ERC721._safeMint(msg.sender, totalTokens);
  }

  function bid(uint tokenId, uint amount) public {
    require(tokenId < ERC721Enumerable.totalSupply(), "Index out of bounds");
    require(msg.sender != ownerOf(tokenId), "Owner cannot bid");
    require(!_music[tokenId].isSold, "Token is already sold");
    require(_music[tokenId].price < amount, "Bid amount should be greater that token price");
    Bid[] storage bids = _bids[tokenId];
    bool bidAgain = false;
    for (uint index = 0; index < bids.length; index++) {
      if (bids[index].biddedBy == msg.sender) {
        _transferAmount(payable(msg.sender), bids[index].amount);
        bids[index].amount = amount;
        bidAgain = true;
      }
    }
    if (!bidAgain)
      bids.push(Bid(bids.length, msg.sender, amount));
  }

  function claim(uint tokenId, uint bidId) public {
    require(tokenId < ERC721Enumerable.totalSupply(), "Index out of bounds");
    require(msg.sender == ownerOf(tokenId), "Only the owner can claim the bids");
    Bid[] memory bids = _bids[tokenId];
    require(bidId < bids.length, "Index out of bounds");
    for (uint index = 0; index < bids.length; index++) {
      if (index == bidId) {
        _transferToken(tokenId, bids[index].biddedBy, bids[index].amount);
      } else {
        _transferAmount(payable(bids[index].biddedBy), bids[index].amount);
      }
    }
  }

  receive() external payable {}
}