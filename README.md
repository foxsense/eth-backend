1. Run the command **npm install -g truffle** to install Truffle CLI
2. This project has setup using [Truffle](https://www.trufflesuite.com/docs/truffle/getting-started/creating-a-project). Used this cmd **truffle init** to setup the create the skeleton app
3. Ganache is used for running local blockchain and testing. Start the Ganache server at any port (eg: http://localhost:7545)
4. Run **truffle compile** to compile the contracts. **build** folder gets generated
5. Run **truffle migrate --reset** to migrate and deploy the contracts, specified in the **migrations** files, to the blockchain
