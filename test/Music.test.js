const Music = artifacts.require('Music');

contract('Music', (accounts) => {
  let contract;

  before(async () => {
    contract = await Music.deployed();
  });

  describe('deployment', async () => {
    it('deploys successfully', async () => {
      const address = contract.address;
      assert.notEqual(address, 0x0);
      assert.notEqual(address, '');
      assert.notEqual(address, null);
      assert.notEqual(address, undefined);
    });

    it('has a name', async () => {
      const name = await contract.name();
      assert.equal(name, 'Music');
    });

    it('has a symbol', async () => {
      const symbol = await contract.symbol();
      assert.equal(symbol, 'MUSIC');
    });
  });

  describe('minting', async () => {
    it('creates a new token', async () => {
      await contract.mint('Eminem');
      const totalSupply = await contract.totalSupply();
      // SUCCESS
      assert.equal(totalSupply, 1);
    });
  });

  describe('indexing', async () => {
    it('lists music', async () => {
      // Mint 2 more tokens
      await contract.mint('Ariana');
      await contract.mint('Michael');
      const totalSupply = await contract.totalSupply();

      const result = [];

      for (var i = 0; i < totalSupply; i++) {
        const music = await contract.music(i);
        result.push(music);
      }

      const expected = ['Eminem', 'Ariana', 'Michael'];
      assert.equal(result.join(','), expected.join(','));
    });
  });
});
